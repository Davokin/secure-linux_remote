Wenn Sie eine neue Maschine einrichten, wird empfohlen, diese zunächst zu sichern. Nehmen wir an, Sie verwenden **Ubuntu 20.04**:

## Erlauben Sie keine SSH-Anmeldung mit Passwort (nur Schlüssel)

> **Stellen Sie zunächst sicher, dass Sie sich tatsächlich [mit Schlüsseln am Server anmelden](https://codeberg.org/Davokin/secure-linux_remote/src/branch/master/create_ssh-rsa.md) und nicht mit einem Passwort, da Sie sich sonst ausschließen.**
{.is-warning}

Stellen Sie zunächst sicher, dass Sie sich tatsächlich mit Schlüsseln am Server anmelden und nicht mit einem Passwort, da Sie sich sonst ausschließen.

Bearbeiten Sie `/etc/ssh/sshd_config` und suchen Sie nach `PasswordAuthentication`. Stellen Sie sicher, dass es auskommentiert ist und auf `no` gesetzt ist. Wenn Sie Änderungen vorgenommen haben, starten Sie den SSH-Dienst neu:

```bash
systemctl restart ssh.service
```

## Systempakete aktualisieren

```bash
apt update && apt upgrade -y
```

## Installieren Sie fail2ban, damit wiederholte Anmeldeversuche blockiert werden

Installieren Sie zuerst fail2ban:

```bash
apt install fail2ban
```

Bearbeiten Sie `/etc/fail2ban/jail.local` und fügen Sie dies ein:

```text
[DEFAULT]
destemail = deine@email.hier
sendername = Fail2Ban

[sshd]
enabled = true
port = 22
mode = aggressive
```

Starten Sie abschließend fail2ban neu:

```bash
systemctl restart fail2ban
```

## Installieren Sie eine Firewall und erlauben Sie nur SSH-, HTTP- und HTTPS-Ports

Installieren Sie zuerst iptables-persistent. Während der Installation wird es Sie fragen, ob Sie die aktuellen Regeln beibehalten möchten – lehnen Sie ab.

```bash
apt install -y iptables-persistent
```

Bearbeiten Sie `/etc/iptables/rules.v4` und fügen Sie dies ein:

```text
*filter

# Erlaube den gesamten Loopback (lo0)-Verkehr und blockiere alle Verbindungen zu 127/8, die nicht lo0 verwenden
-A INPUT -i lo -j ACCEPT
-A INPUT ! -i lo -d 127.0.0.0/8 -j REJECT

# Akzeptiere alle etablierten eingehenden Verbindungen
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# Erlaube den gesamten ausgehenden Verkehr - Sie können dies ändern, um nur bestimmten Verkehr zuzulassen
-A OUTPUT -j ACCEPT

# Erlaube HTTP- und HTTPS-Verbindungen von überall (die normalen Ports für Websites und SSL).
-A INPUT -p tcp --dport 80 -j ACCEPT
-A INPUT -p tcp --dport 443 -j ACCEPT
# (optional) Erlaube HTTP/3-Verbindungen von überall.
-A INPUT -p udp --dport 443 -j ACCEPT

# Erlaube SSH-Verbindungen
# Die -dport-Nummer sollte die gleiche Portnummer sein, die Sie in der sshd_config festgelegt haben
-A INPUT -p tcp -m state --state NEW --dport 22 -j ACCEPT

# Erlaube Ping
-A INPUT -p icmp -m icmp --icmp-type 8 -j ACCEPT

# Erlaube Destination Unreachable-Nachrichten, insbesondere Code 4 (Fragmentierung erforderlich) ist erforderlich oder PMTUD bricht ab
-A INPUT -p icmp -m icmp --icmp-type 3 -j ACCEPT

# Protokolliere iptables abgelehnte Aufrufe
-A INPUT -m limit --limit 5/min -j LOG --log-prefix "iptables denied: " --log-level 7

# Ablehnen aller anderen eingehenden - Standardablehnung, sofern nicht ausdrücklich erlaubt
-A INPUT -j REJECT
-A FORWARD -j REJECT

COMMIT
```

Mit iptables-persistent wird diese Konfiguration beim Booten geladen. Da wir gerade nicht neu starten, müssen wir sie beim ersten Mal manuell laden:

```bash
iptables-restore < /etc/iptables/rules.v4
```

Wenn Ihr Server auch über IPv6 erreichbar ist, bearbeiten Sie `/etc/iptables/rules.v6` und fügen Sie dies ein:
```text
*filter

# Erlaube den gesamten Loopback (lo0)-Verkehr und blockiere alle Verbindungen zu 127/8, die nicht lo0 verwenden
-A INPUT -i lo -j ACCEPT
-A INPUT ! -i lo -d ::1/128 -j REJECT

# Akzeptiere alle etablierten eingehenden Verbindungen
-A INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

# Erlaube den gesamten ausgehenden Verkehr - Sie können dies ändern, um nur bestimmten Verkehr zuzulassen
-A OUTPUT -j ACCEPT

# Erlaube HTTP- und HTTPS-Verbindungen von überall (die normalen Ports für Websites und SSL).
-A INPUT -p tcp --dport 80 -j ACCEPT
-A INPUT -p tcp --dport 443 -j ACCEPT
# (optional) Erlaube HTTP/3-Verbindungen von überall.
-A INPUT -p udp --dport 443 -j ACCEPT

# Erlaube SSH-Verbindungen
# Die -dport-Nummer sollte die gleiche Portnummer sein, die Sie in der sshd_config festgelegt haben
-A INPUT -p tcp -m state --state NEW --dport 22 -j ACCEPT

# Erlaube Ping
-A INPUT -p icmpv6 -j ACCEPT

# Protokolliere iptables abgelehnte Aufrufe
-A INPUT -m limit --limit 5/min -j LOG --log-prefix "iptables denied: " --log-level 7

# Ablehnen aller anderen eingehenden - Standardablehnung, sofern nicht ausdrücklich erlaubt
-A INPUT -j REJECT
-A FORWARD -j REJECT

COMMIT
```
Ähnlich wie bei den IPv4-Regeln können Sie dies manuell laden:
```bash
ip6tables-restore < /etc/iptables/rules.v6
```
