# SSH RSA Key erstellen und auf einen Linux Server kopieren

Diese Anleitung zeigt, wie Sie einen SSH RSA Key erstellen und auf einen Linux Server kopieren können. Der Prozess ist sowohl für Linux als auch für Windows-Benutzer geeignet.

## Schritt 1: SSH Key erstellen

### Für Linux-Benutzer:

1. Öffnen Sie das Terminal.
2. Verwenden Sie den Befehl `ssh-keygen`:

```bash
ssh-keygen -t rsa -b 2048 -f ~/.ssh/id_rsa
```

3. Folgen Sie den Anweisungen, um einen Passphrase festzulegen.

### Für Windows-Benutzer:

1. Installieren Sie Git Bash, falls noch nicht installiert.
2. Öffnen Sie Git Bash und verwenden Sie den Befehl:

```bash
ssh-keygen -t rsa -b 2048 -f ~/.ssh/id_rsa
```

3. Folgen Sie den Anweisungen, um einen Passphrase festzulegen.

## Schritt 2: SSH Key zum Server kopieren

1. Verwenden Sie den Befehl `ssh-copy-id`:

```bash
ssh-copy-id user@server_ip
```

Ersetzen Sie `user` durch Ihren Benutzernamen auf dem Server und `server_ip` durch die IP-Adresse oder den Hostnamen des Servers.

2. Geben Sie Ihr Server-Passwort ein, wenn Sie dazu aufgefordert werden.

## Alternative Schritt 2 für Windows-Benutzer:

1. Öffnen Sie die erstellte SSH-Key-Datei (normalerweise in `C:\Users\YourUsername\.ssh\id_rsa.pub`) mit einem Texteditor.
2. Kopieren Sie den Inhalt der Datei.

3. Melden Sie sich auf Ihrem Linux-Server an.

4. Öffnen Sie die Datei `~/.ssh/authorized_keys` mit einem Texteditor:

```bash
nano ~/.ssh/authorized_keys
```

5. Fügen Sie den zuvor kopierten SSH-Key-Inhalt am Ende der Datei ein.

6. Speichern Sie die Datei und schließen Sie den Editor.

Nach diesen Schritten sollten Sie in der Lage sein, sich ohne Passwort auf Ihren Linux-Server über SSH anzumelden. Stellen Sie sicher, dass der private Schlüssel (`id_rsa`) auf Ihrem lokalen System sicher aufbewahrt wird.
